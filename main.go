package main

import (
	"bitbucket.org/kellydunn/go-trace"
	"container/list"
	"database/sql"
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"
	"time"
)

type Line struct {
	id              uint8
	eventStream     chan *trace.Event
	servingBuffer   *list.List
	servingStarted  chan bool
	servingComplete chan bool

	// TODO: Serial events that have been processed by the
	//       Ruby client should have timestamps at this point in the logs.
	timer           time.Duration
}

func (l *Line) Process(event *trace.Event) {
	fmt.Printf("Processing event on line #%d: %s\n", l.id, string(event.Body()))

	// TODO create a new kegsy serial protocol message
	data := strings.Split(string(event.Body()), ",")
	l.servingBuffer.PushBack(event)

	// TODO Implement
	//      Should filter out when a "serving" has been poured
	//      Algorithm is as follows:
	//        - if we have any large pours outstanding
	//          - if the next event is a pourEnd
	//            - Signal that we need to proccess the Event Buffer.
	//          - else
	//            - Keep appending to the end buffer
	//        - if we have passed the average amount of time to pour a pint
	//          - attempt to grab the last event
	//          - if it is a pourEnd
	//            - Great! this is a typical use case.  Persist to DB.
	//          - if it is a pour
	//            - singal that we have a large outstanding pour
	if len(data) > 4 {
		if data[4] == "pour;" || data[4] == "pourEnd;" {
			l.servingBuffer.PushBack(event)
		}

		if data[4] == "pourEnd;" {
			l.servingComplete <- true
		}
	}
}

func (l *Line) Filter() {
	for {
		canProcess := <-l.servingComplete
		if canProcess {
			
			// TODO:  Formulate the 'message' we want to persist 
			//        To the database.  We should be able to piece
			//        together any intersting data we want to here.
			fmt.Printf("SERVING COMPLETED ON LINE #%d, replaying pour \n", l.id)
			e := l.servingBuffer.Front()
			for e != nil {
				fmt.Printf("  %s\n", string(e.Value.(*trace.Event).Body()))
				l.servingBuffer.Remove(e)
				e = l.servingBuffer.Front()
			}
		}
	}
}

func (l *Line) Handle() {
	for {
		event := <-l.eventStream
		l.Process(event)
	}
}

type Kegsy struct {
	conn        *sql.DB
	messageChan chan *trace.Message
}

func (k *Kegsy) PersistMessage(*trace.Message) error {
	// TODO Implement
	// Should persist messages to database as serving records.
	return nil
}

func (k *Kegsy) Work() {
	for {
		message := <-k.messageChan
		k.PersistMessage(message)
	}
}

type KegsyLogTracer struct{}

func (k *KegsyLogTracer) Trace(lines []*Line) error {
	content, err := ioutil.ReadFile("test/data/arduino-serial.txt")
	events := strings.Split(string(content), "\n")
	for x := range events {
		event := trace.NewEvent([]byte(events[x]))

		// TODO create a new kegsy serial protocol message
		data := strings.Split(string(event.Body()), ",")

		if len(data) > 4 {
			lineId, _ := strconv.Atoi(data[1])
			lines[lineId].eventStream <- event
		}
	}

	return err
}

func (k *KegsyLogTracer) Dispatch(*Line) {

}

func main() {
	l0 := &Line{id: 0, eventStream: make(chan *trace.Event, 100), servingBuffer: list.New(), servingComplete: make(chan bool)}
	l1 := &Line{id: 1, eventStream: make(chan *trace.Event, 100), servingBuffer: list.New(), servingComplete: make(chan bool)}
	l2 := &Line{id: 2, eventStream: make(chan *trace.Event, 100), servingBuffer: list.New(), servingComplete: make(chan bool)}

	k := &Kegsy{messageChan: make(chan *trace.Message)}
	klt := &KegsyLogTracer{}

	go l0.Handle()
	go l0.Filter()
	go l1.Handle()
	go l1.Filter()
	go l2.Handle()
	go l2.Filter()

	go k.Work()

	go klt.Trace([]*Line{l0, l1, l2})

	for {
		time.Sleep(1)
	}
}
